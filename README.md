# Raspberry PI + Yocto manifests
This repository provides Repo manifests to setup the yocto build system and create RPI yocto images

## Installing rpi-yocto

1. Download **repo** tool.

```bash
sudo curl -o /usr/local/bin/repo https://storage.googleapis.com/git-repo-downloads/repo
sudo chmod a+x /usr/local/bin/repo
```

2. Create the installation folder
```
export RPI_YOCTO_SRC=/usr/local/rpi-yocto
sudo install -o <your-user> -g <your-group> -id ${RPI_YOCTO_SRC}
cd ${RPI_YOCTO_SRC}
```

3. Download the needed layers

```bash
repo init -u https://github.com/nikooiko/rpi-yocto-manifests.git -b master
repo sync
```

